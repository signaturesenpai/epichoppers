package com.songoda.epichoppers;

import com.songoda.epichoppers.boost.BoostData;
import com.songoda.epichoppers.boost.BoostManager;
import com.songoda.epichoppers.command.CommandManager;
import com.songoda.epichoppers.economy.Economy;
import com.songoda.epichoppers.economy.PlayerPointsEconomy;
import com.songoda.epichoppers.economy.ReserveEconomy;
import com.songoda.epichoppers.economy.VaultEconomy;
import com.songoda.epichoppers.enchantment.Enchantment;
import com.songoda.epichoppers.handlers.TeleportHandler;
import com.songoda.epichoppers.hopper.Filter;
import com.songoda.epichoppers.hopper.HopperBuilder;
import com.songoda.epichoppers.hopper.HopperManager;
import com.songoda.epichoppers.hopper.levels.Level;
import com.songoda.epichoppers.hopper.levels.LevelManager;
import com.songoda.epichoppers.hopper.levels.modules.*;
import com.songoda.epichoppers.listeners.*;
import com.songoda.epichoppers.player.PlayerDataManager;
import com.songoda.epichoppers.storage.Storage;
import com.songoda.epichoppers.storage.StorageRow;
import com.songoda.epichoppers.storage.types.StorageMysql;
import com.songoda.epichoppers.storage.types.StorageYaml;
import com.songoda.epichoppers.tasks.HopTask;
import com.songoda.epichoppers.utils.*;
import com.songoda.epichoppers.utils.locale.Locale;
import com.songoda.epichoppers.utils.settings.Setting;
import com.songoda.epichoppers.utils.settings.SettingsManager;
import com.songoda.epichoppers.utils.updateModules.LocaleModule;
import com.songoda.update.Plugin;
import com.songoda.update.SongodaUpdate;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


public class EpicHoppers extends JavaPlugin {
    private static CommandSender console = Bukkit.getConsoleSender();

    private static EpicHoppers INSTANCE;

    private ServerVersion serverVersion = ServerVersion.fromPackageName(Bukkit.getServer().getClass().getPackage().getName());

    public Enchantment enchantmentHandler;
    private SettingsManager settingsManager;
    private ConfigWrapper levelsFile = new ConfigWrapper(this, "", "levels.yml");
    private Locale locale;

    private HopperManager hopperManager;
    private CommandManager commandManager;
    private LevelManager levelManager;
    private BoostManager boostManager;
    private PlayerDataManager playerDataManager;

    private Economy economy;

    private TeleportHandler teleportHandler;

    private Storage storage;

    private boolean liquidtanks = false;
    private boolean epicfarming = false;

    public static EpicHoppers getInstance() {
        return INSTANCE;
    }

    @Override
    public void onEnable() {
        INSTANCE = this;

        console.sendMessage(Methods.formatText("&a============================="));
        console.sendMessage(Methods.formatText("&7EpicHoppers " + this.getDescription().getVersion() + " by &5Songoda <3&7!"));
        console.sendMessage(Methods.formatText("&7Action: &aEnabling&7..."));

        this.settingsManager = new SettingsManager(this);
        this.settingsManager.setupConfig();

        // Setup language
        new Locale(this, "en_US");
        this.locale = Locale.getLocale(getConfig().getString("System.Language Mode"));

        // Running Songoda Updater
        Plugin plugin = new Plugin(this, 15);
        plugin.addModule(new LocaleModule());
        SongodaUpdate.load(plugin);

        this.enchantmentHandler = new Enchantment();
        this.hopperManager = new HopperManager();
        this.playerDataManager = new PlayerDataManager();
        this.boostManager = new BoostManager();
        this.commandManager = new CommandManager(this);

        PluginManager pluginManager = Bukkit.getPluginManager();

        // Setup Economy
        if (Setting.VAULT_ECONOMY.getBoolean() && pluginManager.isPluginEnabled("Vault")) {
            // try to load Vault for economy
            if (!((VaultEconomy) (this.economy = new VaultEconomy())).isLoaded())
                this.economy = null; // vault load error
        } else if (Setting.RESERVE_ECONOMY.getBoolean() && pluginManager.isPluginEnabled("Reserve"))
            this.economy = new ReserveEconomy();
        else if (Setting.PLAYER_POINTS_ECONOMY.getBoolean() && pluginManager.isPluginEnabled("PlayerPoints"))
            this.economy = new PlayerPointsEconomy();

        this.loadLevelManager();
        this.checkStorage();

        // Load from file
        loadFromFile();

        new HopTask(this);
        this.teleportHandler = new TeleportHandler(this);

        // Register Listeners
        pluginManager.registerEvents(new HopperListeners(this), this);
        pluginManager.registerEvents(new EntityListeners(this), this);
        pluginManager.registerEvents(new BlockListeners(this), this);
        pluginManager.registerEvents(new InteractListeners(this), this);
        pluginManager.registerEvents(new InventoryListeners(this), this);

        // Check for liquid tanks
        if (pluginManager.isPluginEnabled("LiquidTanks")) liquidtanks = true;

        // Check for epicfarming
        if (pluginManager.isPluginEnabled("EpicFarming")) epicfarming = true;

        // Start auto save
        int saveInterval = Setting.AUTOSAVE.getInt() * 60 * 20;
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, this::saveToFile, saveInterval, saveInterval);

        // Start Metrics
        new Metrics(this);

        console.sendMessage(Methods.formatText("&a============================="));
    }

    @Override
    public void onDisable() {
        this.saveToFile();
        this.storage.closeConnection();

        console.sendMessage(Methods.formatText("&a============================="));
        console.sendMessage(Methods.formatText("&7EpicHoppers " + this.getDescription().getVersion() + " by &5Songoda <3!"));
        console.sendMessage(Methods.formatText("&7Action: &cDisabling&7..."));
        console.sendMessage(Methods.formatText("&a============================="));
    }

    public ServerVersion getServerVersion() {
        return serverVersion;
    }

    public boolean isServerVersion(ServerVersion version) {
        return serverVersion == version;
    }

    public boolean isServerVersion(ServerVersion... versions) {
        return ArrayUtils.contains(versions, serverVersion);
    }

    public boolean isServerVersionAtLeast(ServerVersion version) {
        return serverVersion.ordinal() >= version.ordinal();
    }

    private void checkStorage() {
        if (getConfig().getBoolean("Database.Activate Mysql Support")) {
            this.storage = new StorageMysql(this);
        } else {
            this.storage = new StorageYaml(this);
        }
    }

    /*
     * Saves registered hoppers to file.
     */
    private void saveToFile() {
        // double-check that we're ok to save
        if(EpicHoppers.getInstance().getLevelManager() == null)
            return;

        checkStorage();

        for (Level level : EpicHoppers.getInstance().getLevelManager().getLevels().values())
            for (Module module : level.getRegisteredModules())
                module.saveDataToFile();

        storage.doSave();
    }

    private void loadFromFile() {
        /*
         * Register hoppers into HopperManger from configuration
         */
        Bukkit.getScheduler().runTaskLater(this, () -> {
            if (storage.containsGroup("sync")) {
                for (StorageRow row : storage.getRowsByGroup("sync")) {
                    Location location = Methods.unserializeLocation(row.getKey());
                    if (location == null) return;

                    int levelVal = row.get("level").asInt();
                    Level level = levelManager.isLevel(levelVal) ? levelManager.getLevel(levelVal) : levelManager.getLowestLevel();

                    String playerStr = row.get("player").asString();
                    String placedByStr = row.get("placedby").asString();
                    UUID lastPlayer = playerStr == null ? null : UUID.fromString(row.get("player").asString());
                    UUID placedBy = placedByStr == null ? null : UUID.fromString(placedByStr);

                    List<String> blockLoc = row.get("block").asStringList();
                    List<Location> blocks = new ArrayList<>();
                    if (blockLoc != null) {
                        for (String string : blockLoc) {
                            blocks.add(Methods.unserializeLocation(string));
                        }
                    }

                    Filter filter = new Filter();

                    List<ItemStack> whiteList = row.get("whitelist").asItemStackList();
                    List<ItemStack> blackList = row.get("blacklist").asItemStackList();
                    List<ItemStack> voidList = row.get("void").asItemStackList();

                    String blackLoc = row.get("black").asString();
                    Location black = blackLoc == null ? null : Methods.unserializeLocation(blackLoc);

                    filter.setWhiteList(whiteList);
                    filter.setBlackList(blackList);
                    filter.setVoidList(voidList);
                    filter.setEndPoint(black);

                    TeleportTrigger teleportTrigger = TeleportTrigger.valueOf(row.get("teleporttrigger").asString() == null ? "DISABLED" : row.get("teleporttrigger").asString());

                    hopperManager.addHopper(new HopperBuilder(location)
                            .setLevel(level)
                            .setLastPlayerOpened(lastPlayer)
                            .setPlacedBy(placedBy)
                            .addLinkedBlocks(blocks.toArray(new Location[0]))
                            .setFilter(filter)
                            .setTeleportTrigger(teleportTrigger)
                            .build());
                }
            }

            // Adding in Boosts
            if (storage.containsGroup("boosts")) {
                for (StorageRow row : storage.getRowsByGroup("boosts")) {
                    if (row.get("uuid").asObject() == null)
                        continue;

                    BoostData boostData = new BoostData(
                            row.get("amount").asInt(),
                            Long.parseLong(row.getKey()),
                            UUID.fromString(row.get("uuid").asString()));

                    this.boostManager.addBoostToPlayer(boostData);
                }
            }

            // Save data initially so that if the person reloads again fast they don't lose all their data.
            this.saveToFile();
        }, 10);
    }

    private void loadLevelManager() {
        if (!new File(this.getDataFolder(), "levels.yml").exists())
            this.saveResource("levels.yml", false);

        // Load an instance of LevelManager
        levelManager = new LevelManager();
        /*
         * Register Levels into LevelManager from configuration.
         */
        levelManager.clear();
        for (String levelName : levelsFile.getConfig().getKeys(false)) {
            int level = Integer.valueOf(levelName.split("-")[1]);

            ConfigurationSection levels = levelsFile.getConfig().getConfigurationSection(levelName);

            int radius = levels.getInt("Range");
            int amount = levels.getInt("Amount");
            int linkAmount = levels.getInt("Link-amount", 1);
            boolean filter = levels.getBoolean("Filter");
            boolean teleport = levels.getBoolean("Teleport");
            int costExperiance = levels.getInt("Cost-xp", -1);
            int costEconomy = levels.getInt("Cost-eco", -1);
            int autoSell = levels.getInt("AutoSell");

            ArrayList<Module> modules = new ArrayList<>();

            for (String key : levels.getKeys(false)) {
                if (key.equals("Suction") && levels.getInt("Suction") != 0) {
                    modules.add(new ModuleSuction(this, levels.getInt("Suction")));
                } else if (key.equals("BlockBreak") && levels.getInt("BlockBreak") != 0) {
                    modules.add(new ModuleBlockBreak(this, levels.getInt("BlockBreak")));
                } else if (key.equals("AutoCrafting")) {
                    modules.add(new ModuleAutoCrafting(this));
                } else if (key.equals("AutoSell")) {
                    modules.add(new ModuleAutoSell(this, autoSell));
                }

            }
            levelManager.addLevel(level, costExperiance, costEconomy, radius, amount, filter, teleport, linkAmount, modules);
        }
    }

    public void reload() {
        this.locale = Locale.getLocale(getConfig().getString("System.Language Mode"));
        this.locale.reloadMessages();
        this.settingsManager.reloadConfig();
        loadLevelManager();
    }

    public ItemStack newHopperItem(Level level) {
        ItemStack item = new ItemStack(Material.HOPPER, 1);
        ItemMeta itemmeta = item.getItemMeta();
        itemmeta.setDisplayName(Methods.formatText(Methods.formatName(level.getLevel(), true)));
        String line = getLocale().getMessage("general.nametag.lore").getMessage();
        if (!line.equals("")) {
            itemmeta.setLore(Arrays.asList(line.split("\n")));
        }
        item.setItemMeta(itemmeta);
        return item;
    }

    public Locale getLocale() {
        return locale;
    }

    public TeleportHandler getTeleportHandler() {
        return teleportHandler;
    }

    public BoostManager getBoostManager() {
        return boostManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public LevelManager getLevelManager() {
        return levelManager;
    }

    public HopperManager getHopperManager() {
        return hopperManager;
    }

    public SettingsManager getSettingsManager() {
        return settingsManager;
    }

    public Economy getEconomy() {
        return economy;
    }

    public PlayerDataManager getPlayerDataManager() {
        return playerDataManager;
    }

    public boolean isLiquidtanks() {
        return liquidtanks;
    }

    public boolean isEpicFarming() {
        return epicfarming;
    }

}
